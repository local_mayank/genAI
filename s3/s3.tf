resource "aws_s3_bucket" "example_bucket" {
  bucket = var.s3_bucket.bucket_name
  acl    = "private"  # Specify the desired ACL for the bucket

  # Enable versioning
  versioning {
    enabled = var.s3_bucket.versioning
  }

  # Apply tags
  tags = var.s3_bucket.tags

  # Apply lifecycle rule if configured
  lifecycle_rule {
    enabled = var.s3_bucket.lifecycle_rules[0].enabled

    prefix = var.s3_bucket.lifecycle_rules[0].prefix

    tags = var.s3_bucket.lifecycle_rules[0].tags

    transition {
      days          = var.s3_bucket.lifecycle_rules[0].transitions[0].days
      storage_class = var.s3_bucket.lifecycle_rules[0].transitions[0].storage_class
    }

    expiration {
      days = var.s3_bucket.lifecycle_rules[0].expiration.days
    }
  }
}