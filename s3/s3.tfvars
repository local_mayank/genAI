s3_bucket = {
  bucket_name      = "genais3deployment"
  versioning       = true
  mfa_delete       = false
  lifecycle_rules  = [
    {
      id         = "log"
      enabled    = true
      prefix     = "logs/"
      tags       = {
        autoclean = "true"
        rule      = "log"
      }
      transitions = [
        {
          days          = 30
          storage_class = "GLACIER"
        }
      ]
      expiration = {
        days = 365
      }
    }
  ]
  tags = {
    project     = "Example Project"
    environment = "Development"
  }
}